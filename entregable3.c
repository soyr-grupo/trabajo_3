#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <pthread.h>

#define DIM 20
// declaracion de variables 
int *data;
pid_t *PIDS;
int shmid, t = 1;
int Id_proceso_A;
int Id_proceso_B;
int proceso_a_puede_leer = 0;
int proceso_b_puede_leer = 0;
int espero1=1,espero=1;
// funciones
void manejador_senial(int signum);
void manejador_SIGUSR1(int signum);
void manejador_SIGUSR2(int signum);
int cifrar(int valor, int a, int b, int n);
int descifrar(int valor, int k, int c, int n);

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "A") != 0 && strcmp(argv[1], "B") != 0)) {
        printf("Uso: %s [A|B]\n", argv[0]);
        return 1;
    }
// manejador de señales 
    signal(SIGTERM, manejador_senial);
    signal(SIGUSR1, manejador_SIGUSR1);
    signal(SIGUSR2, manejador_SIGUSR2);

    int PROCESO_A = (strcmp(argv[1], "A") == 0);
    int PROCESO_B = !PROCESO_A;
// claves
    key_t key1=ftok("/bin/more",987);
    key_t key2=ftok("/bin/more",765);
//asignacion de memoria 
    int shmid0 = shmget(key1, DIM * sizeof(int), IPC_CREAT | 0666);
    if (shmid0 == -1) {
        perror("Error en shmget");
        return 1;
    }
//asignacion de data como memoria para los valores 
    data = (int *)shmat(shmid0, NULL, 0);
    if (data == (int *)-1) {
        perror("Error en shmat");
        return 1;
    }

    int shmid1 = shmget(key2, 2 * sizeof(pid_t), IPC_CREAT | 0666);
    if (shmid1 == -1) {
        perror("Error en shmget");
        return 1;
    }
//asignacion de PIDS para difernciar los procesos 
    PIDS = (pid_t *)shmat(shmid1, NULL, 0);
    if (PIDS == (pid_t *)-1) {
        perror("Error en shmat");
        return 1;
    }
    printf("Mi PID es :%d\n",getpid());
    if (PROCESO_A){
        PIDS[0]=getpid();
    }else{
        PIDS[1]=getpid();
    }
    printf("El vector de PIDS es [%d,%d]\n",PIDS[0],PIDS[1]);
// asignacion de  claves publicas y privadas 
    int publica_a = 4, publica_b = 5;
    int privada_k = 7;
    int privada_c;
    int n = 27;
   

    while (t != 0) {
        if (PROCESO_A) {
            //escribe proceso a 
            for (int i = 0; i < 10; i++) {
                data[i] = cifrar(i, publica_a, publica_b, n);
                printf("PID_%c:%d   Proceso %c escribe: %d\n", (PROCESO_A ? 'A' : 'B'), getpid(), (PROCESO_A ? 'A' : 'B'), data[i]);
                 sleep(1);
            }
            printf("espero a que B termine de escribir\n");
            while (espero){

                //printf("estoy esperando  \n\n");
            }

	        espero=1;
                kill(PIDS[1], SIGUSR1);
            
           
        } else  {
	     // escribe proceso b 
            for (int i = 10; i < DIM; i++) {
                data[i] = cifrar(i, publica_a, publica_b, n);
                printf("PID_%c:%d   Proceso %c escribe: %d\n", (PROCESO_B ? 'B' : 'A'), getpid(), (PROCESO_B ? 'B' : 'A'), data[i]);
                 sleep(1);
            }
            printf("espero a que A termine de escribir\n");
            kill(PIDS[0], SIGUSR1);    
        while (espero){
                //espera 
            }
            espero=1;
            printf("Proceso B: esperando a que se complete la lectura del proceso A...\n");
           
        }
            //procesos de lectura
        if ( PROCESO_A) {
            printf("El proceso A lee siempre primero\n");
            for (int i = 10; i < DIM; i++) {
                int valor_leido = descifrar(data[i], 7, 19, n);
                printf("PID_A:%d   Proceso A lee: %d\n", getpid(), valor_leido);
                
            }
            kill(PIDS[1],SIGUSR2);
            while (espero1){

            }
          
        } else if (PROCESO_B) {
            printf("estoy esperando que el proceso A termine de leer\n");
            while (espero1){
            }
            espero1=1;
            for (int i = 0; i < 10; i++) {
                int valor_leido = descifrar(data[i], 7 , 19, n);
                printf("PID_B:%d   Proceso B lee: %d\n", getpid(), valor_leido);
            }
           kill(PIDS[0], SIGUSR2);
        }
    }
kill(PIDS[0],SIGKILL);
shmdt(data);
shmdt(PIDS);
shmctl(shmid0, *data,NULL);
shmctl(shmid1, *PIDS,NULL);
printf ("terminado el programa A\n");
    return 0;
}

void manejador_senial(int signum) {
    if (signum == SIGTERM) {
        kill(PIDS[1], SIGKILL);
        printf("Proceso %d recibió la señal SIGTERM. Terminando...\n", getpid());
        t = 0;
        if (shmdt(data) == -1) {
            perror("Error en shmdt");
        }
    
    }
}

int cifrar(int valor, int a, int b, int n) {
    return (a * valor + b) % n;
}

int descifrar(int valor, int k, int c, int n) {
    return (k * valor + c) % n;
}

void manejador_SIGUSR1(int signum) {
    espero=0;
}

void manejador_SIGUSR2(int signum) {
  
        espero1 = 0;
}
