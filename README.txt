# Readme para el Código de Cifrado y Comunicación de Procesos

Este código implementa una comunicación segura entre dos procesos A y B mediante el cifrado y descifrado de mensajes. El proceso A y el proceso B comparten un segmento de memoria compartida para intercambiar datos cifrados.

## Requisitos

Asegúrate de que tienes las siguientes bibliotecas instaladas en tu sistema:

- `stdio.h`
- `stdlib.h`
- `unistd.h`
- `string.h`
- `signal.h`
- `sys/types.h`
- `sys/ipc.h`
- `sys/shm.h`
- `sys/wait.h`
- `pthread.h`

## Compilación

Compila el código de la siguiente manera:

```shell
gcc -o proceso_cifrado proceso_cifrado.c -lpthread



Uso

Ejecuta el programa con un argumento que indique si es el proceso A o el proceso B:./proceso_cifrado [A|B]



    Si se ejecuta como A, el proceso A escribirá primero en el segmento de memoria compartida, y el proceso B leerá primero.
    Si se ejecuta como B, el proceso B escribirá primero en el segmento de memoria compartida, y el proceso A leerá primero.
Funcionamiento

    Los procesos A y B comparten un segmento de memoria compartida para intercambiar datos cifrados.

    Los procesos generan datos cifrados utilizando una función de cifrado cifrar.

    Los procesos esperan a que el otro proceso termine de escribir antes de comenzar a leer. Esto se controla mediante señales SIGUSR1 y SIGUSR2.

    Los procesos leen datos cifrados del segmento de memoria compartida y los descifran utilizando una función de descifrado descifrar.

    La comunicación entre los procesos continúa hasta que uno de ellos recibe una señal SIGTERM, lo que hace que ambos procesos finalicen y liberen los recursos.
